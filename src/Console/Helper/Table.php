<?php

namespace CPTeam\Console\Helper;

/**
 * @package CPTeam\Console\Helper
 */
class Table extends \Symfony\Component\Console\Helper\Table
{
	
	public function render()
	{
		ob_get_clean();
		parent::render();
		
		return ob_get_clean();
	}
}
