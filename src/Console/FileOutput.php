<?php

namespace CPTeam\Console;

use Symfony\Component\Console\Formatter\OutputFormatterInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @package CPTeam\Console
 */
class FileOutput implements OutputInterface
{
	/** @var null */
	private $fileOutputPath = null;
	
	/** @var OutputInterface */
	private $output;
	
	/**
	 * @param OutputInterface $output
	 * @param $fileOutputPath
	 */
	public function __construct(OutputInterface $output, $fileOutputPath)
	{
		$this->output = $output;
		$this->fileOutputPath = $fileOutputPath;
	}
	

	/**
	 * @param array|string $messages
	 * @param bool $newline
	 * @param int $options
	 *
	 * @return mixed
	 */
	public function write($messages, $newline = false, $options = 0)
	{
		$x = $this->output->write($messages, $newline, $options);
		
		if ($this->fileOutputPath) {
			file_put_contents($this->fileOutputPath, $messages . PHP_EOL, FILE_APPEND);
		}
		
		return $x;
	}
	
	/**
	 * @param array|string $messages
	 * @param int $options
	 *
	 * @return mixed
	 */
	public function writeln($messages, $options = 0)
	{
		$x = $this->output->writeln($messages, $options);
		
		if ($this->fileOutputPath) {
			file_put_contents($this->fileOutputPath, $messages . PHP_EOL, FILE_APPEND);
		}
		
		return $x;
	}
	
	/**
	 * @param int $level
	 *
	 * @return mixed
	 */
	public function setVerbosity($level)
	{
		return $this->output->setVerbosity($level);
	}
	
	/**
	 * @return int
	 */
	public function getVerbosity()
	{
		return $this->output->getVerbosity();
	}
	
	/**
	 * @return bool
	 */
	public function isQuiet()
	{
		return $this->output->isQuiet();
	}
	
	/**
	 * @return bool
	 */
	public function isVerbose()
	{
		return $this->output->isVerbose();
	}
	
	/**
	 * @return bool
	 */
	public function isVeryVerbose()
	{
		return $this->output->isVeryVerbose();
		
	}
	
	/**
	 * @return bool
	 */
	public function isDebug()
	{
		return $this->output->isDebug();
	}
	
	/**
	 * @param bool $decorated
	 *
	 * @return mixed
	 */
	public function setDecorated($decorated)
	{
		return $this->output->setDecorated($decorated);
	}
	
	/**
	 * @return bool
	 */
	public function isDecorated()
	{
		return $this->output->isDecorated();
		
	}
	
	/**
	 * @param OutputFormatterInterface $formatter
	 *
	 * @return mixed
	 */
	public function setFormatter(OutputFormatterInterface $formatter)
	{
		return $this->output->setFormatter($formatter);
	}
	
	/**
	 * @return OutputFormatterInterface
	 */
	public function getFormatter()
	{
		return $this->output->getFormatter();
	}
	
}
