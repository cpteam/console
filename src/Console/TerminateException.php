<?php declare(strict_types = 1);

namespace CPTeam\Console;

class TerminateException extends LogicException
{
}
